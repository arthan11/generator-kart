var VerticalCard = {
  border_radius: 30,
  width: 490,
  height: 750
}

var HorizontalCard = {
  border_radius: 30,
  width: 750,
  height: 490
}

var BigCard = {
  width: 1500,
  height: 1200,
  preview_scale: 0.5
}

var DragonTokenElement = {
  border_radius: 177,
  width: 354,
  height: 354
}
