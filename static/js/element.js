function Element(divId) {
  this.dpi = 300;
  this.source = null;
  this.layout = null;
  this.text_fields = [];
  this.layers = [];

  this.dividers = [];
  this.skills = [];

  this.loading_txt = 'loading'

  this.setDebugMode = function(debug) {
    this.game.debug_mode = debug;
  }

  this.getTextField = function(fieldname) {
    for (var field of this.text_fields) {
      if (field.layer.name == fieldname) {
        return field;
        break;
      }
    }
  }

  this.getLayer = function(layername) {
    for (var layer of this.layers) {
      if (layer.name == layername) {
        return layer;
        break;
      }
    }
  }

  this.replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }
  
  this.setText = function(fieldname, text) {
    var field = this.getTextField(fieldname);
    if (field) {
      text = this.replaceAll(text, '<pz>', '<b><i>Pancerz</i></b>');
      field.fontStyles = [];
      field.fontWeights = [];
		  
	  var i = 0;
	  var in_tag = false;
	  var pos1 = 0;
	  var pos2 = 0;  
	  while (i < text.length) {
		var chr = text[i];
		if (chr == '<') {
		  in_tag = true;
		  pos1 = i;
		};
		if ((chr == '>')&&(in_tag)) {
		  pos2 = i+1;
		  in_tag = false;
		  var tag = text.slice(pos1, pos2);
		  text = text.slice(0, pos1) + text.slice(pos2, text.length);
		  //alert(text);
		  tag = tag.slice(1, tag.length-1);
		  var params = tag.split(' ');
		  tag = params.shift();
		  //alert('tag:'+ tag);
		  //alert('params:' + params);
		  if(tag == 'b') {
			field.addFontWeight('bold', pos1);
		  } else if(tag == '/b') {
			field.addFontWeight('normal', pos1);
		  } else if(tag == 'i') {
			field.addFontStyle('italic', pos1);
		  } else if(tag == '/i') {
			field.addFontStyle('normal', pos1);
		  } else if(tag == 'colour') {
			field.addColor(params[0], pos1);
		  } else if(tag == '/colour') {
			field.addColor('black', pos1);
		  }
		  i -= pos2 - pos1;
		};
		
		//alert(chr);
		i++;
	  }
	  
	  field.text = text;
      this.CalcFontSize(field, field.layer.font_max_size);
    }
  }

  this.setLayout = function(src) {
    this.setSource(src.type);
    this.layout = src;
  }

  this.setSource = function(src) {
    this.source = src;

    if (src.border_radius==null) {
      this.setBorderRadius(0);
    } else {
      this.setBorderRadius(src.border_radius);
    }

    if (src.border==null) {
      this.setBorder(1);
    } else {
      this.setBorder(src.border);
    }

    var scale = 1.0;
    if (src.preview_scale) {
      scale = src.preview_scale;
    }
    
    this.size(src.width, src.height, scale);
    this.reload();
  }

  this.setBorderRadius = function(radius){
    $(this.game.canvas).css("border-radius", radius + "px");
  }

  this.setBorder = function(size){
    $(this.game.canvas).css("border", size + "px solid black");
  }

  this.size = function(width, height, scale) {
    this.game.div.width(width*scale);
    this.game.div.height(height*scale);
    this.game.scale.setGameSize(width, height);
    this.drawBackground();
  }

  this.drawBackground = function(size=14) {
    var bg = this.game.background;
    bg.clear();
    bg.lineStyle(0, 0x0000FF, 1);
    for (var j = 0; j < this.game.height; j += size) {
      for (var i = 0; i < this.game.width; i += size) {
        if (((i/size)%2==0&&((j/size)%2==0))||((i/size)%2==1&&((j/size)%2==1))) {
          bg.beginFill(0x909090);
        } else {
          bg.beginFill(0xe0e0e0);
        };
        bg.drawRect(i, j, size, size);
        bg.endFill();
      }
    }
    this.game.background = bg;
  }

  this.reload = function() {
    this.game.state.start('preload');
  }

  this.onLoadImages = function() {
    if (this.layout&&this.layout.layers) {
      for (var layer of this.layout.layers) {
        if (layer.type!='img') {
          continue;
        }
        this.game.load.image(layer.name, layer.img);
      }
    }
  }

  this.showPicture = function(img, layer_name) {
    var bmd = element.game.add.bitmapData(img.width, img.height);
    bmd.ctx.drawImage(img, 0, 0);

    var layer = element.getLayer(layer_name);
    if (layer) {
      layer.loadTexture(bmd);
      layer.x = layer.layer.x;
      layer.y = layer.layer.y;
      if (layer.x) {
        if(typeof(layer.x)=='string'&&layer.x=='center') {
          layer.x = this.game.width / 2;
        }
      }
      if (layer.y) {
        if(typeof(layer.y)=='string'&&layer.y=='center') {
           layer.y = this.game.height / 2;
        }
      }
    
      if (element.dpi&&element.dpi!=this.dpi) {
        layer.scale.setTo(this.dpi/element.dpi);
      }
      if (layer.layer.after_load_max_width && layer.layer.after_load_max_height) {
        var max_width = layer.layer.after_load_max_width;
        var max_height = layer.layer.after_load_max_height;
        var max_ratio = max_width / max_height;
        var ratio = layer.width / layer.height;
        if (ratio <= max_ratio) {
          layer.scale.setTo( max_height / (layer.height / layer.scale.y) );
        } else {
          layer.scale.setTo( max_width / (layer.width / layer.scale.x) );
        }
      }
      layer.visible = true;
    }
  }
  
  this.readImage = function(input, output) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#'+output).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
    
  this.CalcFontSize = function (txtText, font_max_size=999) {
      if (txtText.text.length == 0) {
        return;
      };
      var region = txtText.region;
      if (txtText.layer.word_wrap == true) {
        while ((txtText.height < region.height)&&(txtText.fontSize < font_max_size)) {
          txtText.fontSize += 1;
        };
        while ((txtText.height > region.height)||(txtText.fontSize > font_max_size)) {
          txtText.fontSize += - 1;
        };
      } else {
        while ((txtText.width < region.width)&&(txtText.height < region.height)&&(txtText.fontSize < font_max_size)) {
          txtText.fontSize += 1;
        };
        while (((txtText.width > region.width)||(txtText.height > region.height))||(txtText.fontSize > font_max_size)) {
          txtText.fontSize += - 1;
        };
      }
    }

  this.doEvent = function(layer, eventName) {
    layer.eventByName(layer, eventName);
  }


  this.onShowImages = function() {
    this.text_fields = [];
    this.layers = [];

    if (this.layout&&this.layout.layers) {
      for (layer of this.layout.layers) {
        if (layer.type=='img') {
          var x = 0;
          var y = 0;
          var anchor_x = 0.0;
          var anchor_y = 0.0;
          if (layer.anchor) {
            anchor_x = layer.anchor[0];
            anchor_y = layer.anchor[1];
          }
          if (layer.x) {
            if(typeof(layer.x)=='string'&&layer.x=='center') {
               x = this.game.width / 2;
               anchor_x = 0.5;
            } else {
              x = layer.x;
            }
          }
          if (layer.y) {
            if(typeof(layer.y)=='string'&&layer.y=='center') {
               y = this.game.height / 2;
               anchor_y = 0.5;
            } else {
              y = layer.y;
            }
          }
          var img = this.game.add.sprite(x, y, layer.name);
          img.name = layer.name;
          img.layer = layer;
          if (layer.dpi&&layer.dpi!=this.dpi) {
            img.scale.setTo(this.dpi/layer.dpi);
          }
          img.anchor.set(anchor_x, anchor_y);
          if (layer.visible!=null) {
            img.visible = layer.visible;
          }
          if (layer.dragable==true) {
            img.inputEnabled = true;
            img.input.useHandCursor = true;
            img.input.enableDrag();
            img.IntervalId = null;
            
            img.moveUp = function() {
              this.y -= 10;
            }
            img.moveDown = function() {
              this.y += 10;
            }
            img.moveLeft = function() {
              this.x -= 10;
            }
            img.moveRight = function() {
              this.x += 10;
            }
            img.collapse = function() {
              if (this.scale.x - 0.02 > 0) {
                this.scale.x -= 0.02;
                this.scale.y -= 0.02;
              }
            }
            img.expand = function() {
              this.scale.x += 0.02;
              this.scale.y += 0.02;
            }
            
            
            img.eventByName = function(layer, eventName) {
              switch(eventName) {
                case 'move_up': this.moveUp(layer);break;
                case 'move_down': this.moveDown(layer);break;
                case 'move_left': this.moveLeft(layer);break;
                case 'move_right': this.moveRight(layer);break;
                case 'collapse': this.collapse(layer);break;
                case 'expand': this.expand(layer);break;
              }
            }
            
            img.setButtonEvent = function(buttonId, eventType) {
              var button = $("#"+buttonId);
              button.attr('layer', this.name);
              button.attr('event', eventType);
              
              button.unbind();
              button.click(function() {
                var layer = element.getLayer(this.getAttribute('layer'));
                var eventType = this.getAttribute('event');
                element.doEvent(layer, eventType);
              }).mousedown(function() {
                var layer = element.getLayer(this.getAttribute('layer'));
                var eventType = this.getAttribute('event');
                layer.IntervalId = setInterval(function() { element.doEvent(layer, eventType); }, 100);
              }).mouseup(function() {
                var layer = element.getLayer(this.getAttribute('layer'));
                clearInterval(layer.IntervalId);
              }).mouseleave(function() {
                var layer = element.getLayer(this.getAttribute('layer'));
                clearInterval(layer.IntervalId);
              });

            }
            
          }
          this.layers.push(img);
        } else if (layer.type=='white_frame') {
          var frame = this.game.add.graphics(0, 0);
          frame.beginFill(0xFFFFFF);
          frame.drawRect(0, 0, this.game.width, layer.rect[1]);
          frame.drawRect(0, this.game.height-layer.rect[3], this.game.width, this.game.height);
          frame.drawRect(0, layer.rect[1], layer.rect[0], this.game.height-layer.rect[1]-layer.rect[3]);
          frame.drawRect(this.game.width-layer.rect[2], layer.rect[1], this.game.width, this.game.height-layer.rect[1]-layer.rect[3]);
          frame.endFill();
        } else if (layer.type=='text') {
          var anchor_x = 0.5;
          var anchor_y = 0.5;
          if (layer.anchor) {
            anchor_x = layer.anchor[0];
            anchor_y = layer.anchor[1];
          }
          var rec = layer.rect;
          var region = new Phaser.Rectangle(rec[0], rec[1], rec[2], rec[3]);
          var color = 'black';
          if (layer.color) {
            color = layer.color;
          }
          //var style = { font: "24px " + layer.font, fill: color, wordWrap: false, wordWrapWidth: rec[2]-rec[0]};
          var word_wrap = (layer.word_wrap == true);
          var style = { font: "24px " + layer.font, fill: color, wordWrap: word_wrap, wordWrapWidth: rec[2]};
          var text_field = this.game.add.text(0, 0, '', style);
          //var text_field = new RichText(this.game, 0, 0, '', style);
          if (this.game.debug_mode == true) {
            text_field.style.backgroundColor = 'red';
          }

          text_field.layer = layer;
          text_field.lineSpacing = -10;
          text_field.anchor.set(anchor_x, anchor_y);
          if (layer.font_bold == true) {
            text_field.fontWeight = 'bold';
          }
          text_field.x = Math.floor(region.x + region.width / 2);
          if (text_field.anchor.y == 0) {
            text_field.y = region.y;
          } else {
            text_field.y = Math.floor(region.y + region.height / 2);
          }
          text_field.text = 'XY';
          text_field.region = region;
          if (layer.angle) {
            text_field.angle = layer.angle;
          }
          if (layer.font_max_size) {
            this.CalcFontSize(text_field, layer.font_max_size);
          } else {
            this.CalcFontSize(text_field);
          }
		  if (layer.align_h != null) {
            text_field.align = layer.align_h;
            text_field.boundsAlignH = layer.align_h;
		  } else {
            text_field.align = "left";
            text_field.boundsAlignH = "left";
		  }
          text_field.boundsAlignV =  "top";
          this.text_fields.push(text_field);
        }
      }
    }
  }


  this.saveAsJPEG = function(fileName) {
    Canvas2Image.saveAsJPEG(this.game.canvas, this.layout.type.width, this.layout.type.height, fileName);
  }

  this.init = function(divId) {
    this.game = new Phaser.Game(490, 750, Phaser.CANVAS, divId, window.devicePixelRatio);
    //this.game.preserveDrawingBuffer = true;
    this.game.element = this;
    this.game.div = $('#'+divId);
    this.game.state.add('boot', Boot);
    this.game.state.add('preload', Preload);
    this.game.state.add('main', Main);
    this.game.state.start('boot');
  }

  this.init(divId);
}


window.onload = function() {
  // wczytywanie czcionek
  document.fonts.load('10pt "Windlass Extended"');//.then(renderText);
  document.fonts.load('10pt "Caxton Extended"');
  document.fonts.load('bold 10pt "Caxton Extended"');
  document.fonts.load('italic 10pt "Caxton Extended"');

  //element = new Element();
};