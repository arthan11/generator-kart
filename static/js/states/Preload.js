class Preload {
  preload() {
    //this.load.image('loading_bg', 'static/img/loading/loading_bg.jpg');
  }
  
  create() {
    this.asset = null;
    this.ready = false;
    //var loading_bg = this.add.sprite(0, 0, 'loading_bg');
    //loading_bg.scale.setTo(2.0);
    var font_size = this.game.width / 10;
    var style = { font: "bold "+font_size+"px Arial", fill: "grey"};
    var loading = this.game.add.text(this.game.width/2, this.game.height/3, this.game.element.loading_txt, style);
    loading.anchor.set(0.5, 0.5);
          
    
    this.asset = this.add.sprite(this.game.width/2, this.game.height/2, 'preloader');
    this.asset.anchor.setTo(0.5, 0.5);
    
    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
    this.load.setPreloadSprite(this.asset);
    
    this.game.element.onLoadImages();
    
    this.load.start();
    
  }
  
  update() {
    if(this.ready) {
      this.game.state.start('main');
    }
  }
  
  onLoadComplete() {
    this.ready = true;
  };

}