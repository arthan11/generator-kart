class Boot {
  init() {
    //this.game.scale.setTo(0.5, 0.5);
    //this.game.scale.setUserScale(0.5, 0.5);
    
    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    //this.game.scale.maxWidth = 500;
    //this.game.scale.maxHeight = 500;
    this.game.scale.refresh();
  }
  
  preload() {
    this.load.image('preloader', 'static/img/loading/loading_bar.png');    
    $(this.game.canvas).css("border-radius", this.game.element.border_radius + "px");
  }
  
  create() {
    this.game.input.maxPointers = 1;
    this.game.state.start('preload');
  }

}