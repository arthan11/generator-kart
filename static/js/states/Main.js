class Main {

  create() {
    this.game.background = this.game.add.graphics(0, 0);
    this.game.element.drawBackground();
    this.game.element.onShowImages();
    if (this.game.element.onAfterLoad) {
      this.game.element.onAfterLoad();
    }
  }

  render() {
    if (this.game.debug_mode) {
      var color = 'rgba(0, 0, 0, 0.4)';
      for (var text_field of this.game.element.text_fields) {
        this.game.debug.geom( text_field.region, color );
      }
    }
  }
}

