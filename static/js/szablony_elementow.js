var CharacterCard = {
  name: 'character',
  type: BigCard,
  layers: [
    {
      type: 'img',
      name: 'back',
      img: 'static/img/layouts/Talisman/character/back.png',
      x: 75,
      y: 73
    },
    {
      type: 'img',
      name: 'background1',
      img: 'static/img/layouts/Talisman/character/character background/Cobbles (basic).png',
      x: 60,
      y: 860,
      visible: false
    },
    {
      type: 'img',
      name: 'background2',
      img: 'static/img/layouts/Talisman/character/character background/deck.png',
      x: 70,
      y: 870,
      visible: false
    },    
    {
      type: 'img',
      name: 'background3',
      img: 'static/img/layouts/Talisman/character/character background/sand.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background4',
      img: 'static/img/layouts/Talisman/character/character background/grass.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background5',
      img: 'static/img/layouts/Talisman/character/character background/mountain.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background6',
      img: 'static/img/layouts/Talisman/character/character background/overgrowth.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background7',
      img: 'static/img/layouts/Talisman/character/character background/path.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background8',
      img: 'static/img/layouts/Talisman/character/character background/street.png',
      x: 70,
      y: 870,
      visible: false
    },
    {
      type: 'img',
      name: 'background9',
      img: 'static/img/layouts/Talisman/character/character background/dungeon.png',
      x: 60,
      y: 850,
      visible: false
    },
    {
      type: 'img',
      name: 'background10',
      img: 'static/img/layouts/Talisman/character/character background/lava.png',
      x: 0,
      y: 800,
      dpi: 150,
      visible: false
    },
    {
      type: 'img',
      name: 'background11',
      img: 'static/img/layouts/Talisman/character/character background/plains.png',
      x: 0,
      y: 800,
      dpi: 150,
      visible: false
    },
    
    {
      type: 'img',
      name: 'picture',
      img: 'static/img/Guard-almogavar-ACR.png',
      x: 408,
      y: 600,
      after_load_max_width: 660,
      after_load_max_height: 900,
      anchor: [0.5, 0.5],
      dragable: true,
      dpi: 320
    },
    {
      type: 'img',
      name: 'front1',
      img: 'static/img/layouts/Talisman/character/front1.png'
    },
    {
      type: 'img',
      name: 'front2',
      img: 'static/img/layouts/Talisman/character/front2.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front3',
      img: 'static/img/layouts/Talisman/character/front3.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front4',
      img: 'static/img/layouts/Talisman/character/front4.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front5',
      img: 'static/img/layouts/Talisman/character/front5.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front6',
      img: 'static/img/layouts/Talisman/character/front6.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front7',
      img: 'static/img/layouts/Talisman/character/front7.png',
      visible: false
    },
    {
      type: 'img',
      name: 'front8',
      img: 'static/img/layouts/Talisman/character/frame.png',
      visible: false
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_icon.png',
      x: 710,
      y: 1054,
      after_load_max_width: 56,
      after_load_max_height: 56,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: false
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 50,
      rect: [476, 40, 544, 68]
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 34,
      rect: [790, 220, 600, 840],
      anchor: [0.5, 0.0],
      word_wrap: true
    },
    {
      type: 'text',
      name: 'strenght',
      font: 'Caxton Extended',
      font_bold: true,
      rect: [44, 450, 40, 40],
      angle: 90
    },
    {
      type: 'text',
      name: 'craft',
      font: 'Caxton Extended',
      font_bold: true,
      rect: [44, 785, 40, 40],
      angle: 90
    },
    {
      type: 'text',
      name: 'fate',
      font: 'Caxton Extended',
      font_bold: true,
      rect: [1420, 308, 40, 40],
      angle: -90
    },
    {
      type: 'text',
      name: 'life',
      font: 'Caxton Extended',
      font_bold: true,
      rect: [1420, 760, 40, 40],
      angle: -90
    },
    {
      type: 'text',
      name: 'starting_and_character',
      font: 'Caxton Extended',
      rect: [810, 1060, 580, 36]
    },
    {
      type: 'img',
      name: 'divider',
      img: 'static/img/layouts/Talisman/character/divider.png',
      x: 845,
      y: 280,
      visible: false
    }
  ]
}

var DragonToken = {
  name: 'dragon_token',
  type: DragonTokenElement,
  layers: [
    {
      type: 'img',
      name: 'picture',
      img: 'static/img/barbarian.jpg',
      x: 177,
      y: 177,
      after_load_max_width: 230,
      after_load_max_height: 230,
      anchor: [0.5, 0.5],
      dragable: true,
      dpi: 250
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/dragon_token/dragon_token_a.png',
      //dpi: 250,
      x: 'center',
      y: 'center'
    }
  ]
}

var TerrainCard = {
  name: 'terrain',
  type: HorizontalCard,
  layers: [
    {
      type: 'img',
      name: 'picture',
      img: 'static/img/magiczny_zamek.jpg',
      x: 375,
      y: 245,
      after_load_max_width: 750,
      after_load_max_height: 490,
      anchor: [0.5, 0.5],
      dragable: true,
      dpi: 220
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/terrain/front.png',
      dpi: 150
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 690,
      y: 430,
      after_load_max_width: 40,
      after_load_max_height: 40,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: false
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 40,
      rect: [46, 42, 324, 80]
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [40, 300, 672, 160],
      anchor: [0.5, 0.0],
      word_wrap: true
    }
  ]
}

var SanctuaryHeroCard = {
  name: 'sanctuary_hero',
  type: VerticalCard,
  layers: [
    {
      type: 'img',
      name: 'picture',
      img: 'static/img/barbarian.jpg',
      x: 243,
      y: 120,
      after_load_max_width: 200,
      after_load_max_height: 200,
      anchor: [0.5, 0.5],
      dragable: true,
      dpi: 300
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/sanctuary_hero/front.png',
      dpi: 300
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [42, 242, 403, 80],
      word_wrap: true,
	  color: 'white'
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [50, 320, 390, 346],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  color: 'white',
	  align_h: 'center'
    }
  ]
}

var WarlockCard = {
  name: 'warlock',
  type: VerticalCard,
  layers: [
    {
      type: 'white_frame',
      rect: [33, 30, 33, 30]
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/warlock/front.png',
      x: 'center',
      y: 'center',
      dpi: 300
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 'center',
      y: 675,
      after_load_max_width: 30,
      after_load_max_height: 30,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [60, 60, 360, 120],
      word_wrap: true,
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [50, 210, 388, 455],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  align_h: 'center'
    }
  ]
}

var QuestRewardCard = {
  name: 'quest_reward',
  type: VerticalCard,
  layers: [
    {
      type: 'white_frame',
      rect: [33, 30, 33, 30]
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/quest_reward/front.png',
      x: 'center',
      y: 'center',
      dpi: 300
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 'center',
      y: 675,
      after_load_max_width: 30,
      after_load_max_height: 30,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [60, 56, 360, 60],
      word_wrap: true,
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [50, 144, 388, 520],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  align_h: 'center'
    }
  ]
}

var PathCard = {
  name: 'path',
  type: VerticalCard,
  layers: [
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/path/front.png',
      x: 'center',
      y: 'center',
      dpi: 300
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 'center',
      y: 666,
      after_load_max_width: 30,
      after_load_max_height: 30,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [62, 56, 364, 56],
      word_wrap: true,
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [62, 132, 364, 526],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  align_h: 'center'
    }
  ]
}

var DestinyCard = {
  name: 'destiny',
  type: VerticalCard,
  layers: [
    {
      type: 'img',
      name: 'bg_l',
      img: 'static/img/layouts/Talisman/destiny/front_l.png',
      x: 'center',
      y: 'center',
      dpi: 300,
      visible: true
    },
    {
      type: 'img',
      name: 'bg_d',
      img: 'static/img/layouts/Talisman/destiny/front_d.png',
      x: 'center',
      y: 'center',
      dpi: 300,
      visible: false
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 'center',
      y: 675,
      after_load_max_width: 30,
      after_load_max_height: 30,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [62, 56, 364, 56],
      word_wrap: true,
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [55, 128, 376, 536],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  align_h: 'center'
    }
  ]
}

var OmenCard = {
  name: 'omen',
  type: HorizontalCard,
  layers: [
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/omen/front.png',
      dpi: 150
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 695,
      y: 455,
      after_load_max_width: 40,
      after_load_max_height: 40,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'type',
      font: 'Windlass Extended',
      font_max_size: 40,
      rect: [110, 14, 533, 80],
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 30,
      rect: [80, 138, 592, 45],
    },
    {
      type: 'text',
      name: 'set',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [216, 95, 318, 45],
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [60, 180, 632, 305],
      anchor: [0.5, 0.0],
      word_wrap: true
    }
  ]
}

var OmenCardByJon = {
  name: 'omen_jon',
  type: HorizontalCard,
  layers: [
    {
      type: 'white_frame',
      rect: [0, 0, 0, 5]
    },
    {
      type: 'img',
      name: 'bg2',
      img: 'static/img/layouts/Talisman/omen/front2.png',
      dpi: 200
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 682,
      y: 415,
      after_load_max_width: 40,
      after_load_max_height: 40,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: true
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 40,
      rect: [110, 52, 533, 70],
    },
    {
      type: 'text',
      name: 'type',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [216, 122, 318, 42],
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [66, 160, 620, 270],
      anchor: [0.5, 0.0],
      word_wrap: true
    }
  ]
}

var AdventureCard = {
  name: 'adventure',
  type: VerticalCard,
  layers: [
    {
      type: 'img',
      name: 'picture',
      img: 'static/img/magiczny_zamek.jpg',
      x: 230,
      y: 290,
      after_load_max_width: 490,
      after_load_max_height: 400,
      anchor: [0.5, 0.5],
      dragable: true,
      dpi: 350
    },
    {
     type: 'white_frame',
     rect: [33, 30, 33, 30]
    },
    {
      type: 'img',
      name: 'bg',
      img: 'static/img/layouts/Talisman/adventure/front.png',
      x: 'center',
      y: 'center',
      dpi: 300
    },
    {
      type: 'img',
      name: 'exp_icon',
      img: 'static/img/exp_symbols/game-icons/anchor.png',
      x: 55,
      y: 450,
      after_load_max_width: 30,
      after_load_max_height: 30,
      anchor: [0.5, 0.5],
      dpi: 300,
      visible: false
    },
    {
      type: 'text',
      name: 'title',
      font: 'Windlass Extended',
      font_max_size: 36,
      rect: [62, 60, 364, 60],
      word_wrap: true,
    },
    {
      type: 'text',
      name: 'type',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [135, 436, 214, 35],
    },
    {
      type: 'text',
      name: 'text',
      font: 'Caxton Extended',
      font_max_size: 30,
      rect: [50, 475, 385, 220],
      anchor: [0.5, 0.0],
      word_wrap: true,
	  align_h: 'center'
    },
    {
      type: 'text',
      name: 'encounter_nr',
      font: 'Caxton Extended',
      font_max_size: 36,
      color: 'white',
      rect: [390, 652, 40, 40],
    }
  ]
}